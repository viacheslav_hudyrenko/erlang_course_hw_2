-module(bs03).
-export([split/2]).

split(BinText, Divider) ->
	lists:reverse(split(BinText, Divider, [<<>>])).

split(<<>>, Divider, Acc) ->
	Acc;

split(<<Divider, Rest/binary>>, Divider, Acc) ->
	split(Rest, Divider, [<<>> | Acc]);

split(<<Symbol, Rest/binary>>, Divider, [Word | T]) ->
	split(Rest, Divider, [<<Word/binary, Symbol>> | T]).
